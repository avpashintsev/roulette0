﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Coinspaid.Entities
{
    public class GetListOfBalancesData
    {
        [JsonProperty(PropertyName = "currency")]
        public string Currency { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "balance")]
        public string Balance { get; set; }
    }

    public class GetListOfBalancesResponse : ApiResponse
    {
        [JsonProperty(PropertyName = "data")]
        public List<GetListOfBalancesData> Data { get; set; }
    }

    public class GetListOfBalancesRequest : IApiRequest
    {        
    }
}
