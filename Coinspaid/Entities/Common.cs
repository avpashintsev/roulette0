﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Coinspaid.Entities
{
    public interface IApiRequest
    {
    }

    public abstract class ApiResponse
    {
        public ErrorData Error { get; set; }
    }

    public class ErrorData
    {
        [JsonProperty(PropertyName = "error")]
        public string Error { get; set; }

        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }
    }
}
