﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Roulette.Domain.Core;
using Roulette_backend.Helpers;
using Roulette_backend.Interface;
using Roulette_backend.Model;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Roulette_backend.Services
{
    public class UserService : IUserService
    {
        readonly IRouletteRepository repository;

        private readonly AppSettings _appSettings;

        public UserService(IOptions<AppSettings> appSettings, IRouletteRepository repo)
        {
            _appSettings = appSettings.Value;
            repository = repo;
        }

        public AuthenticateResponse Authenticate(AuthenticateRequest model)
        {
            var user = new User() { Email = "test", PasswordHash = "password", Id = 4 };
                //await repository.GetUser(model.Email, model.Password);

            // return null if user not found
            if (user == null) return null;

            // authentication successful so generate jwt token
            var token = generateJwtToken(user);

            return new AuthenticateResponse(user, token);

        }

        public User GetById(int id)
        {
            return repository.Users.FirstOrDefault(x => x.Id == id);
        }

        private string generateJwtToken(User user)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", user.Id.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
