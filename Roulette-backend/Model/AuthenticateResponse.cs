﻿using Roulette.Domain.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Roulette_backend.Model
{
    public class AuthenticateResponse
    {
        public int Id { get; set; }
        public string Nickname { get; set; }
        public int AvatarNumber { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }

        public AuthenticateResponse(User user, string token)
        {
            Id = user.Id;
            Nickname = user.Nickname;
            AvatarNumber = user.AvatarNumber;
            Email = user.Email;
            Token = token;
        }
    }
}
