﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Roulette_backend.Model
{
    public class ResponseData
    {
        public Data data { get; set; }

        public static ResponseData Create(object data, string message, string status)
        {
            return new ResponseData() { data = new Data() { data = data, status = status, message = message } };
        }
    }

    public class Data
    {
        public object data { get; set; }
        public string message { get; set; }
        public string status { get; set; }
    }
}
