﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Roulette_backend.Model
{
    public class Balance
    {
		public int Id { get; set; }
		public int UserId { get; set; }
		public int BTC { get; set; }
		public int ETH { get; set; }
	}
}
