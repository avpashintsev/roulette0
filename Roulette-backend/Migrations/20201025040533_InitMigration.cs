﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Roulette_backend.Migrations
{
    public partial class InitMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Email = table.Column<string>(nullable: true),
                    PasswordHash = table.Column<string>(nullable: true),
                    IsEmailConfirm = table.Column<bool>(nullable: false),
                    ReferralKey = table.Column<string>(nullable: true),
                    TwoFactorSecret = table.Column<string>(nullable: true),
                    IsTwoFactorEnabled = table.Column<bool>(nullable: false),
                    ReferralUserId = table.Column<int>(nullable: false),
                    IsBot = table.Column<bool>(nullable: false),
                    PhoneNumber = table.Column<string>(nullable: true),
                    Nickname = table.Column<string>(nullable: true),
                    AvatarNumber = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
