﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Coinmarketcap.Models
{
    public class Response
    {
        [JsonProperty("mins")]
        public int Mins { get; set; }
        [JsonProperty("price")]
        public string Price { get; set; }
    }
}
